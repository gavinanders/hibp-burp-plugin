'''
Created on 29 Jan 2018

@author: gavin
'''
from burp import IBurpExtender
from burp import IScannerCheck
from burp import IScanIssue
from java.net import URL
import urllib2
import json
import re
import time
import sys
from threading import Thread
from array import array

EMAIL_REGEX = re.compile(r'[a-z0-9._]+@([-a-z0-9]+)(\.[-a-z0-9]+)+', re.IGNORECASE)
RATELIMIT_REGEX = re.compile(r'Retry-After:\s\d', re.IGNORECASE)
PWNED_API = "https://haveibeenpwned.com/api/v2/breachedaccount/"
PWNED_HOST = "haveibeenpwned.com"
PWNED_URI = "/api/breachedaccount/"
USER_AGENT = "HaveIBeenPwned-BurpPlugin/1.0"
NAME = "HaveIBeenPwned Compromised Account"
RATING = "Low"
DESCRIPTION = """<p>The response contained email addresses that were found in a publicly disclosed breach. 
The following email address and list of breach locations were found within the response:</p>"""
ISSUE_BACKGROUND = """<p>A "breach" is an incident where a hacker illegally obtains data from a vulnerable system, 
usually by exploiting weaknesses in the software. All the data in the HaveIBeenPwned site comes from website breaches which 
have been made publicly available. Often these breaches include personal and private information, commonly:- usernames, passwords,
password reminders, etc. Disclosure of company email addresses (whether appearing on-screen or hidden within page source) 
may disclose information that is useful to an attacker; for example, they may represent usernames that can be used at the application's login, 
and they may be used in social engineering attacks against the organization's personnel. Any details contained within a breach could lead to an
attacker logging into the application or provide enough personal details about a user to carry out a successful social engineering attack</p>"""
REMEDIATION = """Consider removing any email addresses that are unnecessary, notify the listed compromised accounts of the potential breach and
ensure common passwords are not in use."""

class BurpExtender(IBurpExtender, IScannerCheck):
    
    def registerExtenderCallbacks(self, callbacks):
        self._callbacks = callbacks
        self._helpers = callbacks.getHelpers()
        callbacks.setExtensionName(NAME)
        callbacks.registerScannerCheck(self)
        
    def doPassiveScan(self, baseRequestResponse):
        #Check if we have already scanned this url
        if self.checkAlreadyScanned(baseRequestResponse) == False:
            body = baseRequestResponse.getResponse().tostring()
            
            # get list of emails
            emails = []
            for match in EMAIL_REGEX.finditer(body):
                email = match.group()
                emails.append(email)
            emails = list(set(emails)) #unique list
            
            # return early if no emails
            if len(emails) == 0:
                return None
            
            print emails
            
            # Thread here so we arnt waiting for scan to return
            t = BackgroundCheckEmails(emails, baseRequestResponse, self._callbacks, self._helpers)
            t.setDaemon(True)
            t.start()
            print "Check emails thread started"
    
    def checkAlreadyScanned(self, baseResponse):
        baseurl = self._helpers.analyzeRequest(baseResponse).getUrl().toString()
        issueurl = baseurl.replace(":80", "")
        issueurl = issueurl.replace(":443", "")
        for i in self._callbacks.getScanIssues(issueurl):
            if str(i.getIssueName()) == NAME and str(i.getUrl()) == baseurl:
                print "Issue already raised"
                return True
        return False

    def consolidateDuplicateIssues(self, existing, new):
        if existing.getIssueName() == new.getIssueName():
            return -1
        return 0

class ScanIssue (IScanIssue):
    def __init__(self, httpService, url, httpMessages, name, severity, issue_background, remediation):
        self._httpService = httpService
        self._url = url
        self._httpMessages = httpMessages
        self._name = name
        self._detail = ""
        self._severity = severity
        self._issue_background = issue_background
        self._remediation = remediation

    def getUrl(self):
        return self._url

    def getIssueName(self):
        return self._name

    def getIssueType(self):
        return 0

    def getSeverity(self):
        return self._severity

    def getConfidence(self):
        return "Certain"

    def getIssueBackground(self):
        return self._issue_background

    def getRemediationBackground(self):
        pass

    def getIssueDetail(self):
        return self._detail

    def getRemediationDetail(self):
        return self._remediation

    def getHttpMessages(self):
        return self._httpMessages

    def getHttpService(self):
        return self._httpService
    
    def setDescription(self, email, breaches):
        desc = "%s<p><i>%s</i></p><ul>" % (DESCRIPTION, email)
        for b in breaches:
            desc = desc + "<li>%s</li>" % str(b)
        desc = desc + "</ul>"
        self._detail = desc
    
class BackgroundCheckEmails(Thread):
    def __init__(self, emails, baseRequestResponse, callbacks, helpers):
        self.emails = emails
        self.baseRequestResponse = baseRequestResponse
        self.callbacks = callbacks
        self.helpers = helpers
        Thread.__init__(self)
    
    def _checkPwned(self, email):
        try:
            url = URL(PWNED_API + email)
            req = self.helpers.buildHttpRequest(url)
            rawresp = self.callbacks.makeHttpRequest(url.getHost(), 443, True, req)
            strresp = self.callbacks.getHelpers().bytesToString(rawresp)
            resp = self.helpers.analyzeResponse(rawresp)
            body = strresp[resp.getBodyOffset():]

            if int(resp.getStatusCode()) == 429:
                # make the request again
                headerstring = ' '.join(resp.getHeaders())
                match = RATELIMIT_REGEX.search(headerstring)
                if match:
                    wait = int(str(match.group(0)).split(":")[1].strip())
                    print "Trying again in %i secs" % wait
                    time.sleep(wait)
                    rawresp = self.callbacks.makeHttpRequest(url.getHost(), 443, True, req)
                    strresp = self.callbacks.getHelpers().bytesToString(rawresp)
                    resp = self.helpers.analyzeResponse(rawresp)
                    body = strresp[resp.getBodyOffset():]
                    return json.loads(body)
            elif int(resp.getStatusCode()) == 200:
                return json.loads(body)
            elif int(resp.getStatusCode()) == 503:
                print "Looks like we got blocked by Cloudflare's DoS"
        except Exception, e:
            print e
        return []
    
    def run(self):
        for email in self.emails:
            breaches = self._checkPwned(email)
            if len(breaches) > 0:
                print "%s - %i breaches " % (email, len(breaches))
                httpservice = self.baseRequestResponse.getHttpService()
                url = self.helpers.analyzeRequest(self.baseRequestResponse).getUrl()
                httpmsgs = [self.callbacks.applyMarkers(self.baseRequestResponse, None, None)]
                
                issue = ScanIssue(httpservice, 
                                  url,
                                  httpmsgs,
                                  NAME,
                                  RATING,
                                  ISSUE_BACKGROUND,
                                  REMEDIATION
                                )
                issue.setDescription(email, breaches)
                self.callbacks.addScanIssue(issue)
                time.sleep(2)
            time.sleep(0.5)
